Imports Excel = Microsoft.Office.Interop.Excel
Imports Peloton.AppFrame.IO
Imports Peloton.AppFrame.Interfaces

Public Class clsImport
    Private mclsGeneral As New clsGeneral
    Private mIO As IOEngine = modGlobal.IOEngine

    Private Const msDelimiter As Char = "~"c
    Private mbReplaceExistingData As Boolean
    Private miExcelRowColor As Integer

    Public Sub New(rowColor As Integer, replaceExistingData As Boolean)
        miExcelRowColor = rowColor
        mbReplaceExistingData = replaceExistingData
    End Sub

    Public Function ImportExcel_ImportWellSheet(ByRef xlSheet As Excel.Worksheet, ByRef dicMappings As Dictionary(Of String, String), ByRef dicWells As Dictionary(Of String, String),
                                            ByRef iCompleted As Long, ByRef iWarnings As Long, ByRef iErrors As Long, ByVal sWorkbookName As String, _
                                             ByVal iRowHeading As Integer, ByVal iRowStart As Integer, ByVal iColWellIdentifier As Long) As String

        Dim sErrMessage As String = ""

        Dim frmProgress As New ProgressForm

        Try
            frmProgress.barProgress.Value = 0
            frmProgress.lblFile.Text = "File: " + sWorkbookName
            frmProgress.lblProgress.Text = "Importing Sheet: " + xlSheet.Name + " - Initializing Import..."
            frmProgress.Show()

            Dim sSection As String = "Initialization"
            If dicMappings.Count > 0 Then
                'separate the tables into columns
                Dim dicWellHeader As New Dictionary(Of String, Integer)
                'Dim dicWellbore As New Dictionary(Of String, Integer)
                Dim sTable As String = ""
                Dim dicGroupTable As New Dictionary(Of String, Dictionary(Of String, Integer))
                'Dim dicGroupTableChild As New Dictionary(Of String, Dictionary(Of String, Integer))
                Dim dicTable As New Dictionary(Of String, Integer)
                'Dim dicTableChild As New Dictionary(Of String, Integer)

                For iCol As Integer = 1 To xlSheet.UsedRange.Columns.Count
                    If xlSheet.Cells(iRowHeading, iCol).Text.ToString.Length > 0 AndAlso dicMappings.ContainsKey(xlSheet.Cells(iRowHeading, iCol).Text.ToString) Then
                        Dim sSplitDelimiter As String() = dicMappings.Item(xlSheet.Cells(iRowHeading, iCol).Text.ToString).ToString.Split("|"c)
                        For iSplit As Integer = 0 To sSplitDelimiter.Length - 1
                            Dim sSplitTable As String() = sSplitDelimiter(iSplit).Split("."c)
                            If sSplitTable.Length >= 2 AndAlso sSplitTable(0).Length > 0 AndAlso sSplitTable(0).Length > 1 Then
                                Select Case sSplitTable(0).ToLower.Trim
                                    Case "wvwellheader"
                                        If Not dicWellHeader.ContainsKey(sSplitTable(1).ToLower.Trim) Then dicWellHeader.Add(sSplitTable(1).ToLower.Trim, iCol)
                                        'Case dm.Wellbore.Name
                                        '    If Not dicWellbore.ContainsKey(sSplitTable(1).ToLower.Trim) Then dicWellbore.Add(sSplitTable(1).ToLower.Trim, iCol)
                                    Case sTable
                                        MappingByGroup(dicGroupTable, sSplitTable, iCol)
                                    Case Else
                                        If sTable.Length = 0 Then
                                            sTable = sSplitTable(0).ToLower.Trim
                                            MappingByGroup(dicGroupTable, sSplitTable, iCol)
                                        Else
                                            'non matching table name found (i.e. more than 1 table specified)
                                        End If
                                End Select
                            End If
                        Next
                    End If
                Next


                Dim dicBlank As New Dictionary(Of String, String)
                Dim sErrMessageRow As String = ""
                For iRow As Integer = iRowStart To xlSheet.UsedRange.Rows.Count
                    Try
                        sErrMessageRow = ""
                        frmProgress.barProgress.Value = CInt(100 * iRow / xlSheet.UsedRange.Rows.Count)
                        frmProgress.lblProgress.Text = "Importing Sheet: " + xlSheet.Name + " - Row #" + iRow.ToString
                        frmProgress.Refresh()

                        sSection = "Headers Setup"
                        Dim sWellIdentifier As String = ""
                        Dim sIDWell As String = ""
                        Dim bImport As Boolean = False
                        If xlSheet.Cells(iRow, iColWellIdentifier).Text.ToString.Length > 0 Then
                            '--- check to see if well has been selected to be imported ---
                            sWellIdentifier = xlSheet.Cells(iRow, iColWellIdentifier).Value.ToString
                            If dicWells.ContainsKey(sWellIdentifier.ToLower) Then
                                bImport = True
                                sIDWell = dicWells.Item(sWellIdentifier.ToLower)
                                If sIDWell.Length = 0 Then
                                    'if no IDWell then add new well
                                    sIDWell = mIO.AddEntity
                                End If
                            End If
                        ElseIf Not CheckExcelBlankRow(xlSheet, iRow) Then
                            sErrMessageRow += "Well Identifier Not Found! Sheet " + xlSheet.Name + " - Row #" + iRow.ToString + Environment.NewLine
                        End If

                        'import well header
                        If bImport Then
                            If dicWellHeader.Count > 1 Then
                                ImportTable(xlSheet, dicWellHeader, iRow, sIDWell, sIDWell, sIDWell, "wvwellheader", dicBlank)
                            End If

                            sSection = "Importing Table"
                            If dicGroupTable.Count > 0 Then
                                For Each oKeyGroup As KeyValuePair(Of String, Dictionary(Of String, Integer)) In dicGroupTable
                                    dicTable = oKeyGroup.Value
                                    If dicTable.Count > 0 Then
                                        ImportTable(xlSheet, dicTable, iRow, sIDWell, sIDWell, "", sTable.ToLower, dicBlank)
                                    End If
                                Next
                            End If
                            sErrMessage += "Successfully Imported: " + sWellIdentifier + " - Row #" + iRow.ToString + Environment.NewLine
                            iCompleted += 1
                        End If

                    Catch ex As Exception
                        sErrMessageRow += "Application Error Occurred: Row #" + iRow.ToString + " - Section " + sSection + " - Error " + ex.Message + Environment.NewLine
                        xlSheet.Rows(iRow).Interior.ColorIndex = miExcelRowColor
                        sErrMessage += sErrMessageRow
                        iErrors += 1
                    Finally

                    End Try
                Next
            End If

            frmProgress.Close()

            Return sErrMessage
        Catch ex As Exception
            sErrMessage += "Application Error Occurred: " + ex.Message
            Return sErrMessage
        Finally
            If Not frmProgress.IsDisposed Then
                frmProgress.Dispose()
            End If
        End Try

    End Function

    Public Function ImportExcel_ImportPadSheet(ByRef xlSheet As Excel.Worksheet, ByRef dicMappings As Dictionary(Of String, String), importWells As Dictionary(Of Integer, String), _
                                            ByRef iCompleted As Long, ByRef iWarnings As Long, ByRef iErrors As Long, ByVal sWorkbookName As String, _
                                             ByVal iRowHeading As Integer, ByVal iRowStart As Integer, ByVal iColWellIdentifier As Long, iColWellsPerPad As Integer, iColStartingWellNumber As Integer, wellNumberPrefix As String) As String
        Dim sErrMessage As String = ""
        Dim frmProgress As New ProgressForm

        Try
            frmProgress.barProgress.Value = 0
            frmProgress.lblFile.Text = "File: " + sWorkbookName
            frmProgress.lblProgress.Text = "Importing Sheet: " + xlSheet.Name + " - Initializing Import..."
            frmProgress.Show()

            Dim sSection As String = "Initialization"
            If dicMappings.Count > 0 Then
                Dim dicWellHeader As New Dictionary(Of String, Integer)
               
                For iCol As Integer = 1 To xlSheet.UsedRange.Columns.Count
                    If xlSheet.Cells(iRowHeading, iCol).Text.ToString.Length > 0 AndAlso dicMappings.ContainsKey(xlSheet.Cells(iRowHeading, iCol).Text.ToString) Then
                        Dim sSplitDelimiter As String() = dicMappings.Item(xlSheet.Cells(iRowHeading, iCol).Text.ToString).ToString.Split("|"c)
                        For iSplit As Integer = 0 To sSplitDelimiter.Length - 1
                            Dim sSplitTable As String() = sSplitDelimiter(iSplit).Split("."c)
                            If sSplitTable.Length >= 2 AndAlso sSplitTable(0).Length > 0 AndAlso sSplitTable(0).Length > 1 Then
                                Select Case sSplitTable(0).ToLower.Trim
                                    Case "wvwellheader"
                                        If Not dicWellHeader.ContainsKey(sSplitTable(1).ToLower.Trim) Then dicWellHeader.Add(sSplitTable(1).ToLower.Trim, iCol)
                                    Case Else
                                        'non matching table name found (i.e. more than 1 table specified)
                                End Select
                            End If
                        Next
                    End If
                Next

                Dim sErrMessageRow As String = ""
                Dim excelRow, importCount As Integer
                Dim idWell As String
                Dim wellIdentifier As String
                Dim wellsPerPad, startingWellNumber As Integer
                Dim newWellName As String

                For Each importRow As KeyValuePair(Of Integer, String) In importWells
                    sSection = "Row Import"
                    excelRow = importRow.Key
                    wellIdentifier = xlSheet.Cells(excelRow, iColWellIdentifier).Text.ToString
                    wellsPerPad = CInt(xlSheet.Cells(excelRow, iColWellsPerPad).Text)
                    startingWellNumber = CInt(xlSheet.Cells(excelRow, iColStartingWellNumber).Text)

                    Dim WVTable As Table = mIO.Tables("wvwellheader")
                    Dim WVTableData As TableData
                    Dim WVRecord As TableDataRecord
                    Dim updateRecord As Boolean = False

                    Try
                        sErrMessageRow = ""
                        frmProgress.barProgress.Value = CInt(100 * importCount / importWells.Count)
                        frmProgress.lblProgress.Text = "Importing Sheet: " + xlSheet.Name + " - Row #" + excelRow.ToString
                        frmProgress.Refresh()

                        For iWellCount As Integer = 1 To wellsPerPad
                            newWellName = xlSheet.Cells(excelRow, dicWellHeader("wellname")).Value.ToString & wellNumberPrefix & startingWellNumber.ToString
                            idWell = GetWellIdByName(newWellName)
                            If String.IsNullOrEmpty(idWell) Then idWell = mIO.AddEntity

                            WVTableData = WVTable.GetData(idWell)
                            WVRecord = WVTableData.Records.Item(idWell)

                            For Each oKey As KeyValuePair(Of String, Integer) In dicWellHeader
                                Dim cellValue As Object = xlSheet.Cells(excelRow, oKey.Value).Value
                                If cellValue Is Nothing Then Continue For
                                cellValue = cellValue.ToString

                                If oKey.Key.ToLower = "wellname" Then cellValue = newWellName

                                UpdateTableRecordField(WVTable.Fields(oKey.Key.ToLower), WVRecord, CStr(cellValue))
                                updateRecord = True
                            Next

                            If updateRecord Then
                                WVRecord.ItemRaw("systag") = modGlobal.Tag
                                WVTableData.Update(modGlobal.Tag, True)

                                sErrMessage += "Successfully Imported: " + wellIdentifier + " - Row #" + excelRow.ToString + Environment.NewLine
                                iCompleted += 1
                            End If

                            startingWellNumber += 1
                        Next
                    Catch ex As Exception
                        sErrMessageRow += "Application Error Occurred: Row #" + excelRow.ToString + " - Section " + sSection + " - Error " + ex.Message + Environment.NewLine
                        xlSheet.Rows(excelRow).Interior.ColorIndex = miExcelRowColor
                        sErrMessage += sErrMessageRow
                        iErrors += 1
                    Finally
                        WVRecord = Nothing
                        WVTableData = Nothing
                        WVTable = Nothing
                    End Try

                    importCount += 1
                Next
            End If

            frmProgress.Close()

            Return sErrMessage
        Catch ex As Exception
            sErrMessage += "Application Error Occurred: " + ex.Message
            Return sErrMessage
        Finally
            If Not frmProgress.IsDisposed Then
                frmProgress.Dispose()
            End If
        End Try
    End Function

    Private Function CheckExcelBlankRow(ByRef xlSheet As Excel.Worksheet, ByVal iRow As Integer) As Boolean
        'check to see if entire row is empty - if so, ignore - otherwise, output error message
        Dim bBlank As Boolean = True
        For iCol As Integer = 1 To xlSheet.UsedRange.Columns.Count
            If xlSheet.Cells(iRow, iCol).Text.ToString.Trim.Length > 0 Then
                bBlank = False
                Exit For
            End If
        Next
        Return bBlank
    End Function

    Private Sub MappingByGroup(ByRef dicGroup As Dictionary(Of String, Dictionary(Of String, Integer)), ByVal sSplitTable As String(), ByVal iCol As Integer)
        Dim dicTable As New Dictionary(Of String, Integer)
        Dim dicDefault As New Dictionary(Of String, Integer)

        Dim sGrouping As String = "GroupDefault"
        Dim sGroupingField As String = sSplitTable(1).Trim
        If sSplitTable.Length >= 3 Then
            'groupings
            sGrouping = sSplitTable(1).ToLower.Trim
            sGroupingField = sSplitTable(2).Trim
            For iCount As Integer = 3 To sSplitTable.Length - 1
                sGroupingField += "." + sSplitTable(iCount)
            Next
        End If
        If dicGroup.ContainsKey(sGrouping) Then
            dicTable = dicGroup(sGrouping)
        End If
        If Not dicTable.ContainsKey(sGroupingField) Then
            dicTable.Add(sGroupingField, iCol)
            dicGroup(sGrouping) = dicTable
        End If
    End Sub

    Private Function FieldValue(ByVal sFindField As String, ByVal sStartTable As String, ByVal sIDWell As String, ByVal sIDRec As String) As String
        Dim sValue As String = sFindField + "+"
        Dim sSplit() As String = sValue.Split("+"c)
        sValue = ""
        For iCount As Integer = 0 To sSplit.Length - 1
            If sSplit(iCount).Trim.StartsWith("'") And sSplit(iCount).Trim.EndsWith("'") Then
                sValue += sSplit(iCount).Trim.Substring(1, sSplit(iCount).Trim.Length - 2)
            ElseIf sSplit(iCount).Length > 0 Then
                Dim sFields() As String = sSplit(iCount).Split("."c)
                Dim WVParentTable As Table = mIO.Tables(sStartTable)
                Dim sWVParentRecordID As String = sIDRec
                Do
                    If WVParentTable Is Nothing Then
                        Exit Do
                    Else
                        Dim WVParentTableData As TableData = WVParentTable.GetData(sIDWell)
                        Dim WVParentRecord As TableDataRecord = WVParentTableData.Records.Item(sWVParentRecordID)
                        If sFields(0).Trim.ToLower = WVParentTable.Key Then
                            'current table - set as value
                            sValue += WVParentRecord.ItemConverted(sFields(1).Trim.ToLower).ToString
                            Exit Do
                        Else
                            WVParentTable = WVParentTable.ParentTable
                            sWVParentRecordID = WVParentRecord.ParentId
                        End If
                    End If
                Loop
            End If
        Next
        Return sValue
    End Function

    Private Sub UpdateTableRecordField(ByRef WVField As Field, ByRef WVRecord As TableDataRecord, ByVal sResult As String)
        Select Case WVField.DataType
            Case FieldDataType.String, FieldDataType.StringLong
                Dim iSize As Integer = WVField.Size
                WVRecord.ItemRaw(WVField.Key) = mclsGeneral.Data_ReturnStringUptoMaxLength(sResult, iSize)
            Case FieldDataType.Integer, FieldDataType.Double
                Dim dValue As Double = 0
                If Double.TryParse(sResult, dValue) Then
                    WVRecord.ItemConverted(WVField.Key) = dValue
                End If
            Case FieldDataType.DateTime
                Dim dDate As DateTime
                If Date.TryParse(sResult, dDate) Then
                    WVRecord.ItemConverted(WVField.Key) = dDate
                End If
            Case FieldDataType.Boolean
                Dim bValue As Boolean
                If Boolean.TryParse(sResult, bValue) Then
                    WVRecord.ItemConverted(WVField.Key) = bValue
                Else
                    Select Case sResult.ToLower
                        Case "1", "yes", "true", "-1"
                            WVRecord.ItemConverted(WVField.Key) = True
                        Case "0", "no", "false"
                            WVRecord.ItemConverted(WVField.Key) = False
                    End Select
                End If
            Case Else
                    WVRecord.ItemConverted(WVField.Key) = sResult
        End Select
    End Sub

    Private Function ImportTable(ByRef xlSheet As Excel.Worksheet, ByRef dicTable As Dictionary(Of String, Integer), ByVal iRow As Integer, _
        ByVal sIDWell As String, ByVal sIDRecParent As String, ByVal sIDRec As String, ByVal sTableName As String, _
        ByRef dicIDRecForeign As Dictionary(Of String, String), Optional ByVal bAlwaysUpdate As Boolean = False, Optional ByVal sXRefFields As String = "") As String
        Dim WVTable As Table = mIO.Tables(sTableName)
        Dim WVTableData As TableData = WVTable.GetData(sIDWell)
        Dim WVRecord As TableDataRecord = Nothing

        Dim bUpdate As Boolean = False

        If sIDRec.Length > 0 Then
            WVRecord = WVTableData.Records.Item(sIDRec)
            'record already exists, do not delete 
            bUpdate = True
        Else
            WVRecord = WVTableData.Records.Add(sIDRecParent)
        End If

        If dicIDRecForeign.Count > 0 Then
            For Each oForeign As KeyValuePair(Of String, String) In dicIDRecForeign
                Dim sValue() As String = oForeign.Value.Split(msDelimiter)
                If sValue.Length = 1 Then
                    WVRecord.ItemRaw(oForeign.Key.ToLower) = sValue(0)
                ElseIf sValue.Length >= 2 Then
                    WVRecord.ItemRaw(oForeign.Key.ToLower + "tk") = sValue(0)
                    WVRecord.ItemRaw(oForeign.Key.ToLower) = sValue(1)
                End If
            Next
        End If

        Dim lstXRefFields As New List(Of String)
        If sXRefFields.Length > 0 Then
            Dim sXRefFieldsList As String() = sXRefFields.Split(msDelimiter)
            For iFields As Integer = 0 To sXRefFieldsList.Length - 1
                If Not lstXRefFields.Contains(sXRefFieldsList(iFields).Trim.ToLower) Then lstXRefFields.Add(sXRefFieldsList(iFields).Trim.ToLower)
            Next
        End If

        Dim lstGroupings As New List(Of String)
        For Each oKey As KeyValuePair(Of String, Integer) In dicTable
            If (oKey.Key.IndexOf("idrec") <> 0) AndAlso _
                Not (xlSheet.Cells(iRow, oKey.Value).Value Is Nothing) AndAlso xlSheet.Cells(iRow, oKey.Value).Value.ToString.Trim.Length > 0 AndAlso _
                (mbReplaceExistingData OrElse WVRecord.ItemRaw(oKey.Key.ToLower).ToString.Length = 0) Then

                If lstXRefFields.Contains(oKey.Key) Then
                    WVRecord.ItemRaw(oKey.Key.ToLower) = GetXRef(sTableName, oKey.Key, xlSheet.Cells(iRow, oKey.Value).Value.ToString.Trim)
                ElseIf oKey.Key.IndexOf("&") >= 0 Then
                    'additional fields that needs to be updated
                    Dim sSplitAdd As String() = oKey.Key.Split("&"c)
                    For iAdd As Integer = 1 To sSplitAdd.Length - 1
                        If sSplitAdd(iAdd).IndexOf("=") >= 0 Then
                            Dim sField As String = sSplitAdd(iAdd).Substring(0, sSplitAdd(iAdd).IndexOf("="))
                            If sField.ToLower = "idrecwellbore" Then
                                Dim dicMatch As New Dictionary(Of String, String)
                                dicMatch.Add("des", sSplitAdd(iAdd).Substring(sSplitAdd(iAdd).IndexOf("=") + 1))
                                WVRecord.ItemConverted(sField.ToLower) = GetIDRecFirstMatch(dicMatch, sIDWell, sIDWell, "wvwellbore", True)
                                WVRecord.ItemRaw(sField.ToLower + "tk") = "wvwellbore"
                            Else
                                WVRecord.ItemConverted(sField.ToLower) = sSplitAdd(iAdd).Substring(sSplitAdd(iAdd).IndexOf("=") + 1)
                            End If
                        End If
                    Next
                    UpdateTableRecordField(WVTable.Fields(sSplitAdd(0).ToLower), WVRecord, xlSheet.Cells(iRow, oKey.Value).Value.ToString)
                    bUpdate = True
                Else
                    UpdateTableRecordField(WVTable.Fields(oKey.Key.ToLower), WVRecord, xlSheet.Cells(iRow, oKey.Value).Value.ToString)
                    bUpdate = True
                End If
            End If
        Next

        Dim sReturnValue As String = ""
        If bUpdate OrElse bAlwaysUpdate Then
            WVRecord.ItemRaw("systag") = modGlobal.Tag
            WVTableData.Update(modGlobal.Tag, True)
            sReturnValue = WVRecord.UniqueId
        Else
            WVTableData.Records.Remove(WVRecord.UniqueId)
        End If

        Return sReturnValue

        WVRecord = Nothing
        WVTableData = Nothing
        WVTable = Nothing

    End Function

    Function GetIDRecMostRecent(ByVal sIDWell As String, ByVal sIDRecParent As String, ByVal sTableName As String, ByVal sDateField As String) As String
        Dim sResult As String = ""

        Dim WVTable As Table = mIO.Tables(sTableName)
        Dim WVTableData As TableData = WVTable.GetData(sIDWell)
        Dim WVRecord As TableDataRecord = Nothing

        'need to determind if record already exists
        If WVTableData.Records.Count > 0 Then
            WVTableData.Records.Sort(sDateField + " DESC")
            sResult = WVTableData.Records(0).UniqueId
        Else
            WVRecord = WVTableData.Records.Add(sIDRecParent)
            WVRecord.ItemRaw("systag") = modGlobal.Tag
            sResult = WVRecord.UniqueId
            WVTableData.Update(modGlobal.Tag, True)
        End If

        Return sResult
    End Function

    Function GetIDRecByDate(ByVal sIDWell As String, ByVal sIDRecParent As String, ByVal sTableName As String, ByVal dDate As Date, _
                            Optional ByVal sDateFieldStart As String = "dttmstart", Optional ByVal sDateFieldEnd As String = "dttmend") As String
        Dim sIDResult As String = ""

        Dim WVTable As Table = mIO.Tables(sTableName)
        Dim WVTableData As TableData = WVTable.GetData(sIDWell)
        Dim WVRecord As TableDataRecord = Nothing

        If sIDRecParent.Length > 0 Then WVTableData.Records.ParentIdFilter = sIDRecParent
        Dim sSQL As String = $"({sDateFieldStart} <= #{dDate.ToString}# OR {sDateFieldStart} IS NULL) AND ({sDateFieldEnd} >= #{dDate.ToString}# OR {sDateFieldEnd} IS NULL)"
        WVTableData.Records.Filter(sSQL)
        If WVTableData.Records.Count > 0 Then
            WVTableData.Records.Sort(sDateFieldStart + " DESC")
            sIDResult = WVTableData.Records(0).UniqueId
        End If

        Return sIDResult
    End Function

    Function GetIDRecByDateNoTime(ByVal sIDWell As String, ByVal sIDRecParent As String, ByVal sTableName As String, ByVal sDateField As String, ByVal dDate As Date) As String
        Dim sIDResult As String = ""

        Dim WVTable As Table = mIO.Tables(sTableName)
        Dim WVTableData As TableData = WVTable.GetData(sIDWell)
        Dim WVRecord As TableDataRecord = Nothing

        If sIDRecParent.Length > 0 Then WVTableData.Records.ParentIdFilter = sIDRecParent
        Dim sSQL As String = $"{sDateField} >= #{dDate.ToShortDateString}# AND {sDateField} < #{dDate.AddDays(1).ToShortDateString}# "
        WVTableData.Records.Filter(sSQL)
        If WVTableData.Records.Count > 0 Then
            sIDResult = WVTableData.Records(0).UniqueId
        End If

        Return sIDResult
    End Function

    Function GetIDRecFirstMatch(ByRef dicMatch As Dictionary(Of String, String), ByVal sIDWell As String, ByVal sIDRecParent As String, ByVal sTableName As String, _
                                Optional ByVal bCreateNew As Boolean = True) As String
        Dim sResult As String = ""

        Dim WVTable As Table = mIO.Tables(sTableName)
        Dim WVTableData As TableData = WVTable.GetData(sIDWell)
        Dim WVRecord As TableDataRecord = Nothing

        If sIDRecParent.Length > 0 Then WVTableData.Records.ParentIdFilter = sIDRecParent
        'need to determind if record already exists
        Dim sSQL As String = ""
        For Each oKey As KeyValuePair(Of String, String) In dicMatch
            If oKey.Value.Length = 0 Then
                sSQL += $" AND {oKey.Key} IS NULL "
            Else
                Select Case WVTable.Fields(oKey.Key.ToLower).DataType
                    Case FieldDataType.Double, FieldDataType.Integer
                        Dim iValue As Integer = 0
                        If Integer.TryParse(oKey.Value, iValue) Then
                            sSQL += $" AND {oKey.Key} = {iValue.ToString}"
                        End If
                    Case FieldDataType.DateTime
                        Dim dDate As DateTime
                        If Date.TryParse(oKey.Value, dDate) Then
                            sSQL += $" AND {oKey.Key} = #{oKey.Value}#"
                        End If
                    Case Else
                        sSQL += $" AND {oKey.Key} = '{oKey.Value}'"
                End Select
            End If

        Next
        If sSQL.Length >= 4 Then WVTableData.Records.Filter(sSQL.Substring(4))
        If WVTableData.Records.Count > 0 Then
            sResult = WVTableData.Records(0).UniqueId
        ElseIf bCreateNew Then
            WVRecord = WVTableData.Records.Add(sIDRecParent)
            For Each oKey As KeyValuePair(Of String, String) In dicMatch
                UpdateTableRecordField(WVTable.Fields(oKey.Key), WVRecord, oKey.Value)
            Next
            WVRecord.ItemRaw("systag") = modGlobal.Tag
            sResult = WVRecord.UniqueId
            WVTableData.Update(modGlobal.Tag, True)
        End If

        Return sResult
    End Function

    Function GetXRef(ByVal sTableName As String, ByVal sTableField As String, ByVal sValue As String) As String
        Dim lstXRef As Dictionary(Of String, String) = mclsGeneral.IniResultCollection("XRef." + sTableName.ToUpper + "." + sTableField.ToUpper)
        If lstXRef.ContainsKey(sValue.ToLower) Then
            Return lstXRef.Item(sValue.ToLower)
        Else
            Return sValue
        End If
    End Function

    Private Function GetWellIdByName(sWellIdentifier As String) As String
        Dim sSQL As String = $"SELECT IDWell, WellName FROM wvWellheader WHERE wellname ='{sWellIdentifier.Replace("'", "''").TrimEnd}'"
        Dim sIDWell As String = ""

        Dim headerIds = mclsGeneral.GetDataTable(sSQL)
        If Not headerIds Is Nothing AndAlso headerIds.Rows.Count > 0 Then
            sIDWell = headerIds.Rows(0)("IDWell").ToString()
        End If

        Return sIDWell
    End Function

End Class
