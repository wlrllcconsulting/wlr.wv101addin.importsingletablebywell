Imports System.Data
Imports System.IO
Imports Peloton.AppFrame.IO
Imports System.Data.Common

Public Class clsGeneral
    Private _iniPath As String

    Public ReadOnly Property IniPath As String
        Get
            If String.IsNullOrEmpty(_iniPath) Then
                _iniPath = Path.ChangeExtension(System.Reflection.Assembly.GetExecutingAssembly().Location, ".ini")
            End If

            Return _iniPath
        End Get
    End Property

    Public Function GetDataTable(commandText As String) As DataTable
        Using connection As IDbConnection = GetConnection()
            Dim factory = GetProviderFactory(connection)
            Using command As IDbCommand = connection.CreateCommand()
                command.CommandText = commandText
                Dim adapter As IDbDataAdapter = factory.CreateDataAdapter
                adapter.SelectCommand = command

                Dim data = New DataSet()
                adapter.Fill(data)

                If data.Tables.Count = 0 Then
                    Return Nothing
                End If

                Return data.Tables(0)
            End Using
        End Using
    End Function

    Public Function ExecuteNonQuery(ByVal commandText As String) As Integer
        Using connection As IDbConnection = GetConnection()
            Using command As IDbCommand = connection.CreateCommand()
                command.CommandText = commandText
                Return command.ExecuteNonQuery()
            End Using
        End Using
    End Function

    Public Function GetConnection() As IDbConnection

        Dim engine As IOEngine = modGlobal.IOEngine
        If engine Is Nothing Then
            Throw New InvalidOperationException("IoEngine is not initialized.")
        End If

        Dim connection As IDbConnection = Nothing
        Dim connectionString As String = ""

        Select Case engine.ConnectionDefinition.Provider
            Case DBMS.Access
                connectionString = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Persist Security Info=False;", engine.ConnectionDefinition.Server)
                connection = New OleDb.OleDbConnection(connectionString)
            Case DBMS.Oracle
                connectionString = String.Format("Data Source={0};", engine.ConnectionDefinition.Server)
                If engine.ConnectionDefinition.Trusted Then
                    connectionString = String.Format("{0}Integrated Security=yes;", connectionString)
                Else
                    connectionString = String.Format("{0}User Id={1};Password={2};Integrated Security=no;", connectionString, engine.ConnectionDefinition.User, engine.ConnectionDefinition.Password)
                End If
                connection = New System.Data.OracleClient.OracleConnection(connectionString)
            Case DBMS.SQLServer, DBMS.SQLExpress
                connectionString = String.Format("Data Source={0};Initial Catalog={1};", engine.ConnectionDefinition.Server, engine.ConnectionDefinition.Database)
                If engine.ConnectionDefinition.Trusted Then
                    connectionString = String.Format("{0}Integrated Security=True;", connectionString)
                Else
                    connectionString = String.Format("{0}User Id={1};Password={2};", connectionString, engine.ConnectionDefinition.User, engine.ConnectionDefinition.Password)
                End If
                connection = New SqlClient.SqlConnection(connectionString)
#If MV42 Then
            Case DBMS.SQLCompact
                'Throw New NotSupportedException("SQL Compact is not supported in this version")
                connectionString = String.Format("Data Source={0};", engine.ConnectionDefinition.Server)
                connection = New SqlServerCe.SqlCeConnection(connectionString)
#End If
            Case Else
                Throw New NotSupportedException(String.Format("{0} is not supported in this version.", engine.ConnectionDefinition.Provider))
        End Select

        Return connection
    End Function

    Public Shared Function GetProviderFactory(connection As IDbConnection) As DbProviderFactory

        ' Check to see if the connection is a native .NET type
        If TypeOf connection Is System.Data.OleDb.OleDbConnection Then
            Return DbProviderFactories.GetFactory("System.Data.OleDb")
        ElseIf TypeOf connection Is System.Data.SqlClient.SqlConnection Then
            Return DbProviderFactories.GetFactory("System.Data.SqlClient")
        ElseIf TypeOf connection Is System.Data.SqlServerCe.SqlCeConnection Then
            Return New System.Data.SqlServerCe.SqlCeProviderFactory()
        ElseIf TypeOf connection Is System.Data.OracleClient.OracleConnection Then
            Return DbProviderFactories.GetFactory("System.Data.OracleClient")
        ElseIf TypeOf connection Is System.Data.Odbc.OdbcConnection Then
            Return DbProviderFactories.GetFactory("System.Data.Odbc")
        End If

        Throw New InvalidOperationException("ProviderFactory not found for connection")
    End Function

    Public Function Data_ReturnStringUptoMaxLength(ByVal value As String, ByVal maxLength As Integer) As String
        If value.Length <= maxLength Then
            Return value
        Else
            Return value.Substring(0, maxLength)
        End If
    End Function

    Public Function Data_RemoveMultipleSpaces(ByVal sString As String) As String
        If sString.IndexOf("  ") <> -1 Then
            sString = Data_RemoveMultipleSpaces(sString.Replace("  ", " "))
        End If
        Return sString
    End Function

    Public Function FileDirectory() As String
        'Gets the current file directory
        Dim file As String = System.Reflection.Assembly.GetExecutingAssembly.GetName().CodeBase
        Dim uriBuild As UriBuilder = New UriBuilder(file)
        file = System.IO.Path.GetDirectoryName(Uri.UnescapeDataString(uriBuild.Path))
        Return file
    End Function

    Public Function IniResultCollection(ByVal sectionName As String) As Dictionary(Of String, String)
        Return parseIni(Me.IniPath, sectionName)
    End Function

    ''' <summary>
    ''' Parses the supplied .INI file by section or keyword and return matching key/value pairs as Dictionary(of string, string)
    ''' </summary>
    ''' <param name="strFilePath">Full file path of the .INI file</param>
    ''' <param name="strSection">Specific section of .INI file to search for. Do not include the brackets.</param>
    ''' <param name="isSection">Optionally specify whether the search is for Section or Keyword</param>
    ''' <returns>"Nothing" is returned if the .INI file can not be found</returns>
    ''' <remarks>This assumes that the .INI file is defined in key/value format such as key=123.</remarks>
    Private Function parseIni(ByVal filePath As String, ByVal sectionName As String, Optional ByVal isSection As Boolean = True) As Dictionary(Of String, String)

        If String.IsNullOrEmpty(filePath) Then
            Throw New ArgumentNullException("filePath")
        End If

        If Not File.Exists(filePath) Then
            Throw New FileNotFoundException("INI file not found.", filePath)
        End If

        Dim sectionKey As String = ""
        Dim sectionMap As New Dictionary(Of String, String)(StringComparer.OrdinalIgnoreCase)

        'Add brackets to the keyword if searching by section
        If isSection = True Then
            sectionKey = String.Format("[{0}]", sectionName.ToLower.Trim)
        Else
            sectionKey = sectionName.ToLower.Trim
        End If

        If String.IsNullOrEmpty(sectionKey) Then
            Return Nothing
        End If

        Try

            Using iniReader = New StreamReader(filePath)
                Dim currentLine As String = String.Empty
                If isSection Then
                    'Read until end of document
                    Do While iniReader.Peek() <> -1
                        currentLine = iniReader.ReadLine.ToLower
                        'Find matched section
                        If currentLine.IndexOf(sectionKey) >= 0 Then
                            'Advance to the first key under the matched section
                            currentLine = iniReader.ReadLine
                            'Find all children key/value pairs until the matched section ends
                            Do Until String.IsNullOrEmpty(currentLine) OrElse currentLine.StartsWith(";") OrElse currentLine.StartsWith("[")
                                Dim pair As String() = currentLine.Split("="c)
                                'Add key/value pairs only if they have value
                                If pair.Length > 1 AndAlso Not String.IsNullOrEmpty(pair(0)) AndAlso Not String.IsNullOrEmpty(pair(1)) Then
                                    sectionMap.Add(pair(0), currentLine.Substring(pair(0).Length + 1).Trim())
                                End If
                                currentLine = iniReader.ReadLine
                            Loop
                        End If
                    Loop
                Else
                    'Read until end of document
                    Do While iniReader.Peek() <> -1
                        currentLine = iniReader.ReadLine.ToLower
                        'Find matching key/value pairs 
                        If currentLine.IndexOf(sectionKey) >= 0 AndAlso Not currentLine.StartsWith(";") AndAlso Not currentLine.StartsWith("[") Then
                            Dim strRaw As String() = Split(currentLine, "=")
                            'Add key/value pairs only if they have value
                            If strRaw.Length > 1 AndAlso Not String.IsNullOrEmpty(strRaw(0)) AndAlso Not String.IsNullOrEmpty(strRaw(1)) Then
                                sectionMap.Add(strRaw(0), currentLine.Substring(strRaw(0).Length + 1).Trim())
                            End If
                        End If
                    Loop
                End If
            End Using

            Return sectionMap
        Catch ex As Exception
            Throw New Exception("Ini File Error: " + ex.Message)
            Return Nothing
        End Try
    End Function

End Class

