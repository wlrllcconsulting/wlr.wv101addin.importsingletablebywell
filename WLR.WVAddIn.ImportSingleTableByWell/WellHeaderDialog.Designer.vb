﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class WellHeaderDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(WellHeaderDialog))
        Me.dgvWellSelect = New System.Windows.Forms.DataGridView()
        Me.WellIdentifier = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WellViewExist = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Action = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Import = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.ExcelRowIndex = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDWell = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btn_cancel = New System.Windows.Forms.Button()
        Me.btn_import = New System.Windows.Forms.Button()
        Me.lbl_intro = New System.Windows.Forms.Label()
        Me.btn_deselectAll = New System.Windows.Forms.Button()
        Me.btn_selectAll = New System.Windows.Forms.Button()
        Me.tp_selectAll = New System.Windows.Forms.ToolTip(Me.components)
        Me.tp_deselectAll = New System.Windows.Forms.ToolTip(Me.components)
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        CType(Me.dgvWellSelect, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvWellSelect
        '
        Me.dgvWellSelect.AllowUserToAddRows = False
        Me.dgvWellSelect.AllowUserToDeleteRows = False
        Me.dgvWellSelect.AllowUserToOrderColumns = True
        Me.dgvWellSelect.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvWellSelect.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvWellSelect.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvWellSelect.ColumnHeadersHeight = 30
        Me.dgvWellSelect.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvWellSelect.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.WellIdentifier, Me.WellViewExist, Me.Action, Me.Import, Me.ExcelRowIndex, Me.IDWell})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvWellSelect.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvWellSelect.Location = New System.Drawing.Point(0, 40)
        Me.dgvWellSelect.Margin = New System.Windows.Forms.Padding(0, 10, 0, 10)
        Me.dgvWellSelect.Name = "dgvWellSelect"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvWellSelect.RowHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvWellSelect.RowHeadersVisible = False
        Me.dgvWellSelect.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvWellSelect.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvWellSelect.Size = New System.Drawing.Size(684, 401)
        Me.dgvWellSelect.TabIndex = 0
        '
        'WellIdentifier
        '
        Me.WellIdentifier.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.WellIdentifier.DefaultCellStyle = DataGridViewCellStyle2
        Me.WellIdentifier.HeaderText = "Well Identifier"
        Me.WellIdentifier.Name = "WellIdentifier"
        Me.WellIdentifier.ReadOnly = True
        '
        'WellViewExist
        '
        Me.WellViewExist.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.WellViewExist.DefaultCellStyle = DataGridViewCellStyle3
        Me.WellViewExist.HeaderText = "In WV"
        Me.WellViewExist.Name = "WellViewExist"
        Me.WellViewExist.ReadOnly = True
        Me.WellViewExist.Width = 62
        '
        'Action
        '
        Me.Action.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.Action.HeaderText = "Action"
        Me.Action.Name = "Action"
        Me.Action.ReadOnly = True
        Me.Action.Width = 62
        '
        'Import
        '
        Me.Import.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.Import.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Import.HeaderText = "Perform Action?"
        Me.Import.Name = "Import"
        Me.Import.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Import.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Import.Width = 107
        '
        'ExcelRowIndex
        '
        Me.ExcelRowIndex.HeaderText = "Excel Row"
        Me.ExcelRowIndex.Name = "ExcelRowIndex"
        Me.ExcelRowIndex.ReadOnly = True
        Me.ExcelRowIndex.Visible = False
        '
        'IDWell
        '
        Me.IDWell.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.IDWell.HeaderText = "wvWellHeader.idwell"
        Me.IDWell.Name = "IDWell"
        Me.IDWell.ReadOnly = True
        Me.IDWell.Visible = False
        '
        'btn_cancel
        '
        Me.btn_cancel.Location = New System.Drawing.Point(609, 0)
        Me.btn_cancel.Margin = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.btn_cancel.MinimumSize = New System.Drawing.Size(75, 30)
        Me.btn_cancel.Name = "btn_cancel"
        Me.btn_cancel.Size = New System.Drawing.Size(75, 30)
        Me.btn_cancel.TabIndex = 1
        Me.btn_cancel.Text = "Cancel"
        Me.btn_cancel.UseVisualStyleBackColor = True
        '
        'btn_import
        '
        Me.btn_import.Location = New System.Drawing.Point(524, 0)
        Me.btn_import.Margin = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.btn_import.MinimumSize = New System.Drawing.Size(75, 30)
        Me.btn_import.Name = "btn_import"
        Me.btn_import.Size = New System.Drawing.Size(75, 30)
        Me.btn_import.TabIndex = 2
        Me.btn_import.Text = "Import"
        Me.btn_import.UseVisualStyleBackColor = True
        '
        'lbl_intro
        '
        Me.lbl_intro.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_intro.AutoSize = True
        Me.lbl_intro.Location = New System.Drawing.Point(0, 0)
        Me.lbl_intro.Margin = New System.Windows.Forms.Padding(0)
        Me.lbl_intro.Name = "lbl_intro"
        Me.lbl_intro.Size = New System.Drawing.Size(614, 30)
        Me.lbl_intro.TabIndex = 3
        Me.lbl_intro.Text = " Please select the well(s) you would like to import."
        Me.lbl_intro.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btn_deselectAll
        '
        Me.btn_deselectAll.Image = CType(resources.GetObject("btn_deselectAll.Image"), System.Drawing.Image)
        Me.btn_deselectAll.Location = New System.Drawing.Point(654, 0)
        Me.btn_deselectAll.Margin = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btn_deselectAll.Name = "btn_deselectAll"
        Me.btn_deselectAll.Size = New System.Drawing.Size(30, 30)
        Me.btn_deselectAll.TabIndex = 2
        Me.btn_deselectAll.UseVisualStyleBackColor = True
        '
        'btn_selectAll
        '
        Me.btn_selectAll.Image = CType(resources.GetObject("btn_selectAll.Image"), System.Drawing.Image)
        Me.btn_selectAll.Location = New System.Drawing.Point(619, 0)
        Me.btn_selectAll.Margin = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btn_selectAll.Name = "btn_selectAll"
        Me.btn_selectAll.Size = New System.Drawing.Size(30, 30)
        Me.btn_selectAll.TabIndex = 1
        Me.btn_selectAll.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel3, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvWellSelect, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(10, 10)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(684, 481)
        Me.TableLayoutPanel1.TabIndex = 5
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel2.AutoSize = True
        Me.TableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.TableLayoutPanel2.ColumnCount = 3
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel2.Controls.Add(Me.btn_import, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.btn_cancel, 2, 0)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 451)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(684, 30)
        Me.TableLayoutPanel2.TabIndex = 6
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel3.AutoSize = True
        Me.TableLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.TableLayoutPanel3.ColumnCount = 3
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel3.Controls.Add(Me.btn_selectAll, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.btn_deselectAll, 2, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.lbl_intro, 0, 0)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel3.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(684, 30)
        Me.TableLayoutPanel3.TabIndex = 6
        '
        'WellHeaderDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(704, 501)
        Me.ControlBox = False
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "WellHeaderDialog"
        Me.Padding = New System.Windows.Forms.Padding(10)
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Well Import Selection"
        CType(Me.dgvWellSelect, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvWellSelect As System.Windows.Forms.DataGridView
    Friend WithEvents btn_cancel As System.Windows.Forms.Button
    Friend WithEvents btn_import As System.Windows.Forms.Button
    Friend WithEvents lbl_intro As System.Windows.Forms.Label
    Friend WithEvents btn_deselectAll As System.Windows.Forms.Button
    Friend WithEvents btn_selectAll As System.Windows.Forms.Button
    Friend WithEvents tp_selectAll As System.Windows.Forms.ToolTip
    Friend WithEvents tp_deselectAll As System.Windows.Forms.ToolTip
    Friend WithEvents WellIdentifier As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WellViewExist As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Action As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Import As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents ExcelRowIndex As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDWell As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
End Class
