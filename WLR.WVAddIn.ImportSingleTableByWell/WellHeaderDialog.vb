﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports Peloton.AppFrame.IO
Imports System.Linq

Public Class WellHeaderDialog
    Private mclsGeneral As New clsGeneral
    Private mIO As IOEngine = modGlobal.IOEngine
    Private mxlApp As Excel.Application = Nothing
    Private mxlWorkBook As Excel.Workbook = Nothing
    Private mxlSheet As Excel.Worksheet = Nothing
    Dim mlstIniMappings As Dictionary(Of String, String)
    Dim msWellIdentifier As String = ""
    Dim miColWellIdentifier As Long = 0
    Dim miRowHeading As Integer = 0
    Dim miRowStart As Integer = 0
    Private msFile As String = ""
    Private m_ImportMode As WellImportMode
    Private m_ExcelRowColor As Integer = 6
    Private m_ReplaceExistingData As Boolean = True
    Private m_WellsPerPadColumn As Integer
    Private m_WellStartNumberColumn As Integer
    Private m_wellNumberPrefix As String

    Public Sub New(ByVal sFile As String, importMode As WellImportMode)
        InitializeComponent()
        msFile = sFile
        m_ImportMode = importMode
    End Sub

    Private Sub WellHeaderDialog_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim settingsKey As String
        Dim tableMappingsKey As String
        Dim sheetName As String = ""
        Dim bAddNewWell As Boolean = False
        Dim sWellIdentifierColName As String = ""
        Dim wellPerPadColumnName As String = ""
        Dim wellStartNumberColumnName As String = ""

        Try
            Select Case m_ImportMode
                Case WellImportMode.Pads
                    settingsKey = "PadSettings"
                    tableMappingsKey = "PadMappings"

                Case WellImportMode.Wells
                    settingsKey = "Settings"
                    tableMappingsKey = "Mappings"

                Case Else
                    Throw New Exception("Import mode not supported.")
            End Select

            mlstIniMappings = mclsGeneral.IniResultCollection(tableMappingsKey)
            If mlstIniMappings Is Nothing Then
                Throw New KeyNotFoundException(String.Format("INI section '{0}' Not Found", tableMappingsKey))
            End If

            Dim lstSettings As Dictionary(Of String, String) = mclsGeneral.IniResultCollection(settingsKey)
            If lstSettings Is Nothing Then
                Throw New KeyNotFoundException(String.Format("INI section '{0}' Not Found", settingsKey))
            End If

            If lstSettings.ContainsKey("ImportTagName") Then modGlobal.Tag = lstSettings.Item("ImportTagName")
            If lstSettings.ContainsKey("SheetName") Then sheetName = lstSettings.Item("SheetName")
            If lstSettings.ContainsKey("AddNewWells") Then Boolean.TryParse(lstSettings.Item("AddNewWells"), bAddNewWell)
            If lstSettings.ContainsKey("HighlightColor") Then m_ExcelRowColor = CInt(lstSettings.Item("HighlightColor"))
            If lstSettings.ContainsKey("ReplaceExistingData") Then Boolean.TryParse(lstSettings.Item("ReplaceExistingData"), m_ReplaceExistingData)
            If lstSettings.ContainsKey("WellsPerPad") Then wellPerPadColumnName = lstSettings.Item("WellsPerPad")
            If lstSettings.ContainsKey("StartingWellNumber") Then wellStartNumberColumnName = lstSettings.Item("StartingWellNumber")
            If lstSettings.ContainsKey("WellNumberPrefix") Then m_wellNumberPrefix = lstSettings.Item("WellNumberPrefix").Trim()

            If String.IsNullOrEmpty(m_wellNumberPrefix) Then
                Throw New NullReferenceException("No well number prefix found in INI.")
            End If

            If Not New Char() {"_"c, "-"c, " "c}.Any(Function(c) m_wellNumberPrefix(0) = c) Then
                m_wellNumberPrefix = " " & m_wellNumberPrefix ' Add a space if no separating character exists first
            End If

            If lstSettings.ContainsKey("WellIdentifier") Then
                sWellIdentifierColName = lstSettings.Item("WellIdentifier")
                If mlstIniMappings.ContainsKey(sWellIdentifierColName) Then
                    msWellIdentifier = mlstIniMappings.Item(sWellIdentifierColName)
                    Dim sSplit() As String = msWellIdentifier.Split("."c)
                    If sSplit.Length >= 1 AndAlso sSplit(0).ToLower = "wvwellheader" Then
                        msWellIdentifier = sSplit(1)
                        Dim WVTable As Table = mIO.TableMain
                        dgvWellSelect.Columns.Item("WellIdentifier").HeaderText = WVTable.Fields(msWellIdentifier.ToLower).CaptionLong
                        WVTable = Nothing
                    Else
                        Throw New Exception("Well Identifier Must Be From WellHeader Table (mapping section of ini file).  Please contact your WellView administrator to correct this issue.")
                    End If
                Else
                    Throw New Exception("Well Identifier Not Found (mapping section of ini file).  Please contact your WellView administrator to correct this issue.")
                End If
            Else
                Throw New Exception("Well Identifier Not Set (ini file).  Please contact your WellView administrator to correct this issue.")
            End If

            If lstSettings.ContainsKey("HeaderRow") AndAlso Not Integer.TryParse(lstSettings.Item("HeaderRow").ToString, miRowHeading) Then
                Throw New Exception("Header Row Not Set To Valid Number (ini file).  Please contact your WellView administrator to correct this issue.")
            End If
            If lstSettings.ContainsKey("ImportStartRow") AndAlso Not Integer.TryParse(lstSettings.Item("ImportStartRow").ToString, miRowStart) Then
                Throw New Exception("Import Start Row Not Set To Valid Number (ini file).  Please contact your WellView administrator to correct this issue.")
            End If

            mxlApp = New Excel.Application
            mxlWorkBook = mxlApp.Workbooks.Open(msFile)

            Dim frmProgress As New ProgressForm
            frmProgress.Text = "Retrieving Well..."

            For Each xlSheet As Excel.Worksheet In mxlWorkBook.Sheets
                If xlSheet.Name = sheetName AndAlso xlSheet.UsedRange.Rows.Count > 1 AndAlso xlSheet.Cells(miRowHeading, 1).Text.ToString.Length > 0 Then
                    'find the well identifier column and other column indexes
                    For iCol As Integer = 1 To xlSheet.UsedRange.Columns.Count
                        If xlSheet.Cells(miRowHeading, iCol).Text.ToString.Length = 0 Then Continue For

                        Select Case xlSheet.Cells(miRowHeading, iCol).Value.ToString.ToLower
                            Case sWellIdentifierColName.ToLower
                                miColWellIdentifier = iCol
                            Case wellPerPadColumnName.ToLower
                                m_WellsPerPadColumn = iCol
                            Case wellStartNumberColumnName.ToLower
                                m_WellStartNumberColumn = iCol
                        End Select
                    Next

                    If miColWellIdentifier = 0 Then Err.Raise(10000, "", "Well Identifier Column is Not Found.  Please Check the Ini File.")

                    frmProgress.barProgress.Value = 0
                    frmProgress.lblFile.Text = "Please wait... Reading Excel File..."
                    frmProgress.lblProgress.Text = "Initializing..."
                    frmProgress.Show()

                    Dim dicWells As New List(Of String)
                    For iRow As Integer = miRowStart To xlSheet.UsedRange.Rows.Count
                        If xlSheet.Cells(iRow, miColWellIdentifier).Text.ToString.Length > 0 Then
                            If Not dicWells.Contains(xlSheet.Cells(iRow, miColWellIdentifier).Value.ToString.ToLower) Or m_ImportMode = WellImportMode.Pads Then 'Pad import will allow duplicate well names
                                dgvWellSelect.Rows.Add(DataGridRow(xlSheet.Cells(iRow, miColWellIdentifier).Value.ToString, msWellIdentifier, bAddNewWell, iRow))
                                dicWells.Add(xlSheet.Cells(iRow, miColWellIdentifier).Value.ToString.ToLower)
                            End If
                        End If
                        frmProgress.barProgress.Value = CInt(100 * iRow / xlSheet.UsedRange.Rows.Count)
                        frmProgress.lblProgress.Text = "Reading Row #" + iRow.ToString + " out of " + xlSheet.UsedRange.Rows.Count.ToString
                        frmProgress.Refresh()
                    Next

                    mxlSheet = xlSheet

                    Exit For
                End If
            Next
            frmProgress.Close()

            For iRow As Integer = 0 To dgvWellSelect.Rows.Count - 1
                If dgvWellSelect.Rows.Item(iRow).Cells("Import").Value = False Then
                    dgvWellSelect.Rows.Item(iRow).Cells("Import").ReadOnly = True
                    dgvWellSelect.Rows.Item(iRow).DefaultCellStyle.BackColor = Drawing.Color.Beige
                End If
            Next

            Me.ControlBox = False
            tp_deselectAll.SetToolTip(btn_deselectAll, "Deselect All")
            tp_selectAll.SetToolTip(btn_selectAll, "Select All")

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show("Error Occurred: " + ex.Message, "Error", System.Windows.Forms.MessageBoxButtons.OK)

            If Not (mxlWorkBook Is Nothing) Then
                mxlWorkBook.Save()
                mxlWorkBook.Close()
            End If

            If Not (mxlApp Is Nothing) Then mxlApp.Quit()

            mxlSheet = Nothing
            mxlWorkBook = Nothing
            mxlApp = Nothing

            Me.Close()
        End Try
    End Sub

    Private Sub btn_ok_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_import.Click
        Try
            Dim oImport As New clsImport(m_ExcelRowColor, m_ReplaceExistingData)
            Dim iCompleted As Long = 0
            Dim iErrors As Long = 0
            Dim iWarnings As Long = 0
            Dim sMessage As String = ""

            Dim dicWells As New Dictionary(Of String, String)
            Dim dicWellsFromPads As New Dictionary(Of Integer, String)

            For iRow As Integer = 0 To dgvWellSelect.Rows.Count - 1
                If CBool(dgvWellSelect.Rows(iRow).Cells("Import").Value) Then
                    If Not dicWells.ContainsKey(dgvWellSelect.Rows(iRow).Cells("WellIdentifier").Value.ToString.ToLower) Then _
                        dicWells.Add(dgvWellSelect.Rows(iRow).Cells("WellIdentifier").Value.ToString.ToLower, dgvWellSelect.Rows(iRow).Cells("IDWell").Value.ToString)

                    dicWellsFromPads.Add(CInt(dgvWellSelect.Rows(iRow).Cells("ExcelRowIndex").Value), dgvWellSelect.Rows(iRow).Cells("IDWell").Value.ToString)
                End If
            Next

            If dicWells.Count > 0 Or dicWellsFromPads.Count > 0 Then
                sMessage = modGlobal.Title + " Started --------------- " + Date.Now.ToString + Environment.NewLine
                Try
                    Select Case m_ImportMode
                        Case WellImportMode.Wells
                            sMessage += oImport.ImportExcel_ImportWellSheet(mxlSheet, mlstIniMappings, dicWells, iCompleted, iWarnings, iErrors, mxlWorkBook.Name, miRowHeading, miRowStart, miColWellIdentifier)
                        Case WellImportMode.Pads
                            sMessage += oImport.ImportExcel_ImportPadSheet(mxlSheet, mlstIniMappings, dicWellsFromPads, iCompleted, iWarnings, iErrors, mxlWorkBook.Name, miRowHeading, miRowStart, miColWellIdentifier, m_WellsPerPadColumn, m_WellStartNumberColumn, m_wellNumberPrefix)
                    End Select
                Catch ex As Exception
                    sMessage += msFile + ": " + ex.Message + Environment.NewLine
                End Try
                sMessage += modGlobal.Title + " Completed --------------- " + Date.Now.ToString + Environment.NewLine

                Dim frmReviewLog As New ReviewLog
                frmReviewLog.LogFile_Create(msFile, iCompleted, iWarnings, iErrors, sMessage)
                Me.Close()
                frmReviewLog.ShowDialog()
            End If

            Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show("Error Occurred: " + ex.Message, "Error", System.Windows.Forms.MessageBoxButtons.OK)
        Finally
            If Not (mxlWorkBook Is Nothing) Then
                mxlWorkBook.Save()
                mxlWorkBook.Close()
            End If

            If Not (mxlApp Is Nothing) Then mxlApp.Quit()

            mxlSheet = Nothing
            mxlWorkBook = Nothing
            mxlApp = Nothing

            Me.Close()
        End Try
    End Sub

    Private Sub btn_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancel.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel

        If Not (mxlWorkBook Is Nothing) Then
            'mxlWorkBook.Save() ' WHY?  Pointless.
            mxlWorkBook.Close()
        End If

        If Not (mxlApp Is Nothing) Then mxlApp.Quit()

        mxlSheet = Nothing
        mxlWorkBook = Nothing
        mxlApp = Nothing

        Me.Close()
    End Sub

    Private Sub btn_selectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selectAll.Click
        toggleCheckbox(True)
    End Sub

    Private Sub btn_deselectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_deselectAll.Click
        toggleCheckbox(False)
    End Sub

    Private Sub toggleCheckbox(ByVal selectAll As Boolean)

        For i As Integer = 0 To Me.dgvWellSelect.Rows.Count - 1

            Me.dgvWellSelect.Item(3, i).Value = selectAll

        Next

    End Sub

    Private Function DataGridRow(ByVal sWellIdentifier As String, ByVal sField As String, ByVal bAddNew As Boolean, excelRowIndex As Integer) As String()
        Dim sResult As String()
        'Dim dsData As New DataSet
        Dim sSQL As String = ""

        Dim sIDWell As String = ""
        Dim sExists As String = "No"
        Dim sAction As String = "No Import"

        If bAddNew Then sAction = "New"

        Select Case m_ImportMode
            Case WellImportMode.Wells
                sSQL = $"SELECT IDWell, WellName FROM wvwellheader WHERE {sField} = '{sWellIdentifier.Replace("'", "''").TrimEnd}'"
            Case WellImportMode.Pads
                sSQL = $"SELECT IDWell, WellName FROM wvwellheader WHERE {sField} LIKE '{sWellIdentifier.Replace("'", "''").TrimEnd}%'"
        End Select

        Dim headerIds = mclsGeneral.GetDataTable(sSQL)
        If Not headerIds Is Nothing AndAlso headerIds.Rows.Count > 0 Then
            If headerIds.Rows.Count = 1 Then
                sIDWell = headerIds.Rows(0).Item("IDWell").ToString
                sExists = "Yes"
                sAction = "Update"
                bAddNew = True
            Else
                sIDWell = "-1" 'multiple ids exist for pad import
                sExists = "Yes"
                sAction = "Update"
                bAddNew = True
            End If
        End If

        sResult = {sWellIdentifier, sExists, sAction, bAddNew.ToString, excelRowIndex.ToString, sIDWell}
        Return sResult
    End Function

End Class