﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ImportDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.diag_browseImportFile = New System.Windows.Forms.OpenFileDialog()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.rdoWells = New System.Windows.Forms.RadioButton()
        Me.rdoPads = New System.Windows.Forms.RadioButton()
        Me.btn_cancel = New System.Windows.Forms.Button()
        Me.btn_import = New System.Windows.Forms.Button()
        Me.groupBox_SelectFile = New System.Windows.Forms.GroupBox()
        Me.lbl_fileLocation = New System.Windows.Forms.Label()
        Me.txt_fileLocation = New System.Windows.Forms.TextBox()
        Me.btn_browse = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.groupBox_SelectFile.SuspendLayout()
        Me.SuspendLayout()
        '
        'diag_browseImportFile
        '
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.AutoSize = True
        Me.TableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.TableLayoutPanel1.ColumnCount = 5
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox1, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.btn_cancel, 3, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.btn_import, 2, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.groupBox_SelectFile, 1, 3)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 7
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(434, 261)
        Me.TableLayoutPanel1.TabIndex = 10
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.SetColumnSpan(Me.GroupBox1, 3)
        Me.GroupBox1.Controls.Add(Me.TableLayoutPanel2)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 13)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(408, 60)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Select Import Type"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 3
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.rdoWells, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.rdoPads, 0, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 16)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(402, 41)
        Me.TableLayoutPanel2.TabIndex = 2
        '
        'rdoWells
        '
        Me.rdoWells.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rdoWells.AutoSize = True
        Me.rdoWells.Location = New System.Drawing.Point(64, 5)
        Me.rdoWells.Margin = New System.Windows.Forms.Padding(5)
        Me.rdoWells.Name = "rdoWells"
        Me.rdoWells.Size = New System.Drawing.Size(51, 31)
        Me.rdoWells.TabIndex = 0
        Me.rdoWells.Text = "Wells"
        Me.rdoWells.UseVisualStyleBackColor = True
        '
        'rdoPads
        '
        Me.rdoPads.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rdoPads.AutoSize = True
        Me.rdoPads.Checked = True
        Me.rdoPads.Location = New System.Drawing.Point(5, 5)
        Me.rdoPads.Margin = New System.Windows.Forms.Padding(5)
        Me.rdoPads.Name = "rdoPads"
        Me.rdoPads.Size = New System.Drawing.Size(49, 31)
        Me.rdoPads.TabIndex = 1
        Me.rdoPads.TabStop = True
        Me.rdoPads.Text = "Pads"
        Me.rdoPads.UseVisualStyleBackColor = True
        '
        'btn_cancel
        '
        Me.btn_cancel.Location = New System.Drawing.Point(329, 224)
        Me.btn_cancel.Name = "btn_cancel"
        Me.btn_cancel.Size = New System.Drawing.Size(92, 24)
        Me.btn_cancel.TabIndex = 8
        Me.btn_cancel.Text = "Cancel"
        Me.btn_cancel.UseVisualStyleBackColor = True
        '
        'btn_import
        '
        Me.btn_import.Location = New System.Drawing.Point(231, 224)
        Me.btn_import.Name = "btn_import"
        Me.btn_import.Size = New System.Drawing.Size(92, 24)
        Me.btn_import.TabIndex = 6
        Me.btn_import.Text = "Import"
        Me.btn_import.UseVisualStyleBackColor = True
        '
        'groupBox_SelectFile
        '
        Me.groupBox_SelectFile.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.SetColumnSpan(Me.groupBox_SelectFile, 3)
        Me.groupBox_SelectFile.Controls.Add(Me.lbl_fileLocation)
        Me.groupBox_SelectFile.Controls.Add(Me.txt_fileLocation)
        Me.groupBox_SelectFile.Controls.Add(Me.btn_browse)
        Me.groupBox_SelectFile.Location = New System.Drawing.Point(13, 89)
        Me.groupBox_SelectFile.Name = "groupBox_SelectFile"
        Me.groupBox_SelectFile.Size = New System.Drawing.Size(408, 112)
        Me.groupBox_SelectFile.TabIndex = 7
        Me.groupBox_SelectFile.TabStop = False
        Me.groupBox_SelectFile.Text = "Select File to Import"
        '
        'lbl_fileLocation
        '
        Me.lbl_fileLocation.AutoSize = True
        Me.lbl_fileLocation.Location = New System.Drawing.Point(7, 32)
        Me.lbl_fileLocation.Name = "lbl_fileLocation"
        Me.lbl_fileLocation.Size = New System.Drawing.Size(92, 13)
        Me.lbl_fileLocation.TabIndex = 3
        Me.lbl_fileLocation.Text = "Import file location"
        '
        'txt_fileLocation
        '
        Me.txt_fileLocation.Location = New System.Drawing.Point(6, 51)
        Me.txt_fileLocation.Name = "txt_fileLocation"
        Me.txt_fileLocation.Size = New System.Drawing.Size(396, 20)
        Me.txt_fileLocation.TabIndex = 1
        '
        'btn_browse
        '
        Me.btn_browse.Location = New System.Drawing.Point(327, 77)
        Me.btn_browse.Name = "btn_browse"
        Me.btn_browse.Size = New System.Drawing.Size(75, 23)
        Me.btn_browse.TabIndex = 2
        Me.btn_browse.Text = "Browse..."
        Me.btn_browse.UseVisualStyleBackColor = True
        '
        'ImportDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(434, 261)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ImportDialog"
        Me.Text = "WLR - Well Import"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.groupBox_SelectFile.ResumeLayout(False)
        Me.groupBox_SelectFile.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents diag_browseImportFile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents rdoWells As System.Windows.Forms.RadioButton
    Friend WithEvents rdoPads As System.Windows.Forms.RadioButton
    Friend WithEvents btn_cancel As System.Windows.Forms.Button
    Friend WithEvents btn_import As System.Windows.Forms.Button
    Friend WithEvents groupBox_SelectFile As System.Windows.Forms.GroupBox
    Friend WithEvents lbl_fileLocation As System.Windows.Forms.Label
    Friend WithEvents txt_fileLocation As System.Windows.Forms.TextBox
    Friend WithEvents btn_browse As System.Windows.Forms.Button
End Class
