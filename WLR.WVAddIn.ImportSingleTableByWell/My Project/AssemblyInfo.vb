﻿
Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices
Imports System.Resources

' General Information
<Assembly: AssemblyTitle("WLR.WVAddIn.ImportSingleTableByWell")>
<Assembly: AssemblyDescription("www.wlr-llc.com")>
<Assembly: AssemblyCompany("Well Lifecycle Resources")>
<Assembly: AssemblyProduct("WLR.WVAddIn.ImportSingleTableByWell")>
<Assembly: AssemblyCopyright("Copyright © WLR 2018")>

' Version information
<Assembly: AssemblyVersion("1.0.2.18205")>
<Assembly: AssemblyFileVersion("1.0.2.18205")>
<Assembly: NeutralResourcesLanguageAttribute("en-US")>

