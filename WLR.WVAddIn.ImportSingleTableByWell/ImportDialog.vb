﻿Public Class ImportDialog
    Private mclsGeneral As New clsGeneral

    Private Sub ImportDialog_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Me.ControlBox = False
        'Me.Visible = False

        'callFileBrowser()

        Me.Visible = True
    End Sub

    Private Sub btn_browse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_browse.Click
        callFileBrowser()
    End Sub

    Private Sub btn_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancel.Click
        Me.Close()
    End Sub

    Private Sub btn_import_Click(sender As System.Object, e As System.EventArgs) Handles btn_import.Click
        If Me.txt_fileLocation.Text.Length > 0 Then
            Dim frmWellHeaderDialog As New WellHeaderDialog(Me.txt_fileLocation.Text, If(rdoWells.Checked, WellImportMode.Wells, WellImportMode.Pads))
            If frmWellHeaderDialog.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                Me.Close()
            End If
        Else
            System.Windows.Forms.MessageBox.Show("Please select file to import.", "Select File", System.Windows.Forms.MessageBoxButtons.OK)
        End If

    End Sub

    Private Sub diag_browseImportFile_FileOk(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles diag_browseImportFile.FileOk
        Me.txt_fileLocation.Text = diag_browseImportFile.FileName.ToString
    End Sub

    Private Sub callFileBrowser()
        diag_browseImportFile.Title = "Please Select a File to Import"
        diag_browseImportFile.Filter = "Excel Files (*.xl*;*.xlsx;*.xlsm;*.xlsb;*.xlam;*.xltx;*.xltm;*.xls;*.xla;*.xlt;*.xlm;*.xlw)|*.xl*;*.xlsx;*.xlsm;*.xlsb;*.xlam;*.xltx;*.xltm;*.xls;*.xla;*.xlt;*.xlm;*.xlw|Text Files (*.txt; *.csv)|*.txt;*.csv"
        diag_browseImportFile.ShowDialog()
    End Sub

End Class