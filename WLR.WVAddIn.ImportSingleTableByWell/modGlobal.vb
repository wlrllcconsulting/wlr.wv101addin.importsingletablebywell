Imports Peloton.AppFrame.IO

Public Module modGlobal
    Private mIO As IOEngine
    Private msIODBConn As String = ""
    Private msTitle As String = "WLR Import Single Table By Well"
    Private msTag As String = "WLRTableImport"
    Private mExportFileDir As String = ""

    Public Property IOEngine() As IOEngine
        Get
            Return mIO
        End Get
        Set(ByVal value As IOEngine)
            mIO = value
            msIODBConn = ConnFromIO()
        End Set
    End Property

    Public ReadOnly Property IODBConnection As String
        Get
            Return msIODBConn
        End Get
    End Property

    Public Property Title() As String
        Get
            Return msTitle
        End Get
        Set(ByVal sValue As String)
            msTitle = sValue
        End Set
    End Property

    Public Property Tag() As String
        Get
            Return msTag
        End Get
        Set(ByVal value As String)
            msTag = value
        End Set
    End Property

    Public Property ExportFileDir() As String
        Get
            Return mExportFileDir
        End Get
        Set(ByVal value As String)
            mExportFileDir = value
        End Set
    End Property

    Private Function ConnFromIO() As String
        'set up the connection
        Dim sConn As String = ""

        If Not mIO Is Nothing Then
            Select Case mIO.ConnectionDefinition.Provider
                Case DBMS.Access
                    sConn = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Persist Security Info=False;", mIO.ConnectionDefinition.Server)
                Case DBMS.Oracle
                    sConn = String.Format("Data Source=MyOracleDB;User Id={0};Password={1};Integrated Security=no;", mIO.ConnectionDefinition.User, mIO.ConnectionDefinition.Password)
                Case DBMS.SQLServer, DBMS.SQLExpress
                    sConn = String.Format("Provider=SQLOLEDB;Server={0};Database={1};", mIO.ConnectionDefinition.Server, mIO.ConnectionDefinition.Database)
                    If mIO.ConnectionDefinition.Trusted Then
                        sConn = String.Format("{0}Trusted_Connection=yes;", sConn)
                    Else
                        sConn = String.Format("{0}UID={1};PWD={2};", sConn, mIO.ConnectionDefinition.User, mIO.ConnectionDefinition.Password)
                    End If
#If MV42 Then
                Case DBMS.SQLCompact
                    sConn = ""
#End If
            End Select
        End If

        Return sConn
    End Function

    Public Enum WellImportMode
        Wells
        Pads
    End Enum

End Module

